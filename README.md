# primaku-frontend-test

## How to run locally

1. clone the repository

    ```bash
    git clone https://gitlab.com/fatahnuram/primaku-frontend-test.git
    cd primaku-frontend-test
    ```

2. install dependencies

    ```bash
    npm install -g @angular/cli
    npm install
    ```

3. run the server locally

    ```bash
    npm run start
    ```

4. access it to your http://localhost:4200/

## How to build docker image

1. clone the repository

    ```bash
    git clone https://gitlab.com/fatahnuram/primaku-frontend-test.git
    cd primaku-frontend-test
    ```

2. build the image

    ```bash
    docker build -t primaku-frontend:v1.0.0 .
    ```

3. run the server locally via docker container

    ```bash
    docker run --rm -p 4200:80 primaku-frontend:v1.0.0
    ```

4. access it to your http://localhost:4200/

5. push the image to the registry

    ```bash
    docker tag primaku-frontend:v1.0.0 fatahnuram/primaku-frontend-test:v0.1.0
    docker push fatahnuram/primaku-frontend-test:v0.1.0
    ```

## How to deploy to Kubernetes

1. clone the repository

    ```bash
    git clone https://gitlab.com/fatahnuram/primaku-frontend-test.git
    cd primaku-frontend-test
    ```

2. upgrade (or install) the helm chart, or

    ```bash
    helm upgrade --install -f ./helm/values.yaml --namespace <mynamespace> primaku-frontend-test ./helm
    ```

3. upgrade (or install) the helm chart and override replicas or container image and tag

    ```bash
    helm upgrade --install -f ./helm/values.yaml --namespace <mynamespace> --set image.repository=nginx --set image.tag=stable --set replicaCount=2 primaku-frontend-test ./helm/
    ```
