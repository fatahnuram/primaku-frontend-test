FROM node:16.16-alpine3.16 as builder

WORKDIR /app
RUN npm install -g @angular/cli
COPY package.json package-lock.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.22.0-alpine
COPY --from=builder /app/dist/primaku-frontend-test/ /usr/share/nginx/html
